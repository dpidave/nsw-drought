function createKey(colors, min_range, max_range) {
    var keyWidth = 400;
    //get rid of anything that is there
    d3.select("#keyBar").remove();
    var key = d3.select("#month")
        .append("svg")
        .attr("id", "keyBar")
        .attr("width", keyWidth)
        .attr("height", 50);


    var legend = key.append("defs")
        .append("svg:linearGradient")
        .attr("id", "gradient")
        .attr("x1", "0%")
        .attr("y1", "100%")
        .attr("x2", "100%")
        .attr("y2", "100%")
        .attr("spreadMethod", "pad");

    if (colors.length == 5) {
        var percent = ["20%", "40%", "60%", "80%", "100%"];
    } else {
        var percent = ["10%", "20%", "30%", "40%", "50%",
            "60%", "70%", "80", "90%", "100%"];
    }
    for (var i=0; i<colors.length; i++) {
        legend.append("stop")
            .attr("offset", percent[i])
            .attr("stop-color", colors[i])
            .attr("stop-opacity", 1);
        console.log(colors[i]);
    }
    key.append("rect")
        .attr("width", keyWidth) // need some space to fit numbers
        .attr("height", 50 - 40) // hight of bar
        .style("fill", "url(#gradient)")
        .attr("transform", "translate(0,11)");

    //xaxis
    var xScale = d3.scale.linear()
        .domain([min_range, max_range])
        .range([5, 380]);

    var xAxis = d3.svg.axis()
        .scale(xScale)
        .orient("bottom")
        .innerTickSize(5)
        .outerTickSize(0)
        .tickPadding(5);

    key.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + 20 + ")")
        .call(xAxis)
        .selectAll("text")
        .attr("dx", ".4em");
}
