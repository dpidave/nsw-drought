// see : http://duspviz.mit.edu/d3-workshop/mapping-data-with-d3/
var width = 900;
var height = 500;

// FIXME: These don't reflect the totals, ie need another array that is [June
// average', Jul Average', Aug Aveage,,] etc for average rainfall data.
var months = ["Jun 2017", "Jul 2017", "Aug 2017", "Sep 2017", "Oct 2017",
    "Nov 2017", "Dec 2017", "Jan 2018", "Feb 2018", "Mar 2018", "Apr 2018",
    "May 2018"];

// color range for normal mean rainfall, greens
var colorMeanRange = ["rgb(237,248,233)", "rgb(186,228,179)",
            "rgb(116,196,118)",
            "rgb(49,163,84)", "rgb(0,109,44)"];
// color range for deficit, reds
var colorDecreaseRange = ['rgb(127,0,0)', 'rgb(179,0,0)', 'rgb(215,48,31)',
            'rgb(239,101,72)', 'rgb(252,141,89)', 'rgb(253, 187,132)',
            'rgb(253,212,158)', 'rgb(254,232,200)', 'rgb(247,255,242)',
            'rgb(222, 255, 221)'];
// create the key initially set to deficit
createKey(colorDecreaseRange, -400, 200);

// radio button selection
var form = document.getElementById('radios');
// starting month
var monthSelect = "Jun 2017";
// when the input range changes update the circle
d3.select("#slideMonth").on("input", function(d) {
    monthSelect = months[this.value];
    var formStart;
    for (var i=0; i<form.length; i++) {
       if (form[i].checked) {
       formStart = form[i].id;
           }
     }
    if (formStart == 'mean') {
        d3.select("#monthLabel").text("Month: " + monthSelect.split(' ')[0]);
        createKey(colorMeanRange, 0, 100);
    } else {
        d3.select("#monthLabel").text("Month: " + monthSelect);
        createKey(colorDecreaseRange, -400, 200);
    }
    changeMonth(monthSelect);
});
// function to change the view once the radio is pushed
d3.select("#radios").on("input", function(d) {
    for (var i=0; i<form.length; i++) {
       if (form[i].checked) {
       formStart = form[i].id;
           }
     }
    if (formStart == 'mean') {
        d3.select("#monthLabel").text("Month: " + monthSelect.split(' ')[0]);
        console.log('here3');
    } else {
        d3.select("#monthLabel").text("Month: " + monthSelect);
    }
    changeMonth(monthSelect);
});
// color for mean
// this to be based on the ranges in the data 0-100?
var colorMean = d3.scale.quantize()
    .range(colorMeanRange);
// color range to show deficit
// could try quantile for more even distribution
var colorDecrease = d3.scale.quantize()
    .range(colorDecreaseRange);

var projection = d3.geo.mercator()
    .scale(2500)
    .center([147, -33])
    .translate([width/2, height /2]);

var path = d3.geo.path().projection(projection);

var svg = d3.select('#mapCanvas')
    .attr('height', height)
    .attr('width', width);

svg.append('rect')
    .attr('class', 'background')
    .attr('width', width)
    .attr('height', height);

var g = svg.append('g');

var mapLayer = g.append('g')
    .classed('map-layer', true);

d3.csv("./static/data/lga_geoIDs_weather_data.csv", function(data) {
    // Set input domain for color scale, could use data but hard coded
    colorMean.domain([0, 100]);
    colorDecrease.domain([-400, 200]);

// load map and add a property for mean rainfall
    d3.json('./static/data/nsw_lga_sm.json', function(error, mapData) {
        if (error) throw error;
        var features = mapData.features;
        for (var i=0; i< data.length; i++) {
            var dataLGA = data[i].name;
            for (var j = 0; j < features.length; j++) {
                var jsonLGA = features[j].properties.NSW_LGA__2;
                if (jsonLGA == dataLGA) {
                    // defines feature properties
                    features[j].properties.Jun = parseInt(data[i].Mean_Jun);
                    features[j].properties.Jul = parseInt(data[i].Mean_Jul);
                    features[j].properties.Aug = parseInt(data[i].Mean_Aug);
                    features[j].properties.Sep = parseInt(data[i].Mean_Sep);
                    features[j].properties.Oct = parseInt(data[i].Mean_Oct);
                    features[j].properties.Nov = parseInt(data[i].Mean_Nov);
                    features[j].properties.Dec = parseInt(data[i].Mean_Dec);
                    features[j].properties.Jan = parseInt(data[i].Mean_Jan);
                    features[j].properties.Feb = parseInt(data[i].Mean_Feb);
                    features[j].properties.Mar = parseInt(data[i].Mean_Mar);
                    features[j].properties.Apr = parseInt(data[i].Mean_Apr);
                    features[j].properties.May = parseInt(data[i].Mean_May);
                    // decline in rainfall since july 17
                    features[j].properties.decJul = parseInt(data[i].Dec_Jul);
                    features[j].properties.decAug = parseInt(data[i].Dec_Aug);
                    features[j].properties.decSep = parseInt(data[i].Dec_Sep);
                    features[j].properties.decOct = parseInt(data[i].Dec_Oct);
                    features[j].properties.decNov = parseInt(data[i].Dec_Nov);
                    features[j].properties.decDec = parseInt(data[i].Dec_Dec);
                    features[j].properties.decJan = parseInt(data[i].Dec_Jan);
                    features[j].properties.decFeb = parseInt(data[i].Dec_Feb);
                    features[j].properties.decMar = parseInt(data[i].Dec_Mar);
                    features[j].properties.decApr = parseInt(data[i].Dec_Apr);
                    features[j].properties.decMay = parseInt(data[i].Dec_May);
                    break;
                    }
            }
        };
        // var features = mapData.features;
        mapLayer.selectAll('path')
            .data(features)
            .enter().append('path')
            .attr('d', path)
            .attr('fill', '#fff')
            .attr('stroke', '#aaa')
            .attr('vector-effect', 'non-scaling-stroke')
            // .style('fill', function(d) {
            //    return colorMean(d.properties.Jun);
            // })
            .on('mouseover', mouseover)
            .on('mouseout', mouseout);
    });
});
/**
 * Creats name string for mouse over
 * @param {obj} d d3 obj.
 * @return {string}
 */
function nameFn(d) {
    var textRain = 0;
    var formVal;
    for (var i=0; i<form.length; i++) {
        if (form[i].checked) {
            formVal = form[i].id;
        }
    }
    if (formVal == 'mean') {
    if (monthSelect == months[0]) {
            textRain = d.properties.Jun;
        }
    if (monthSelect == months[1]) {
            textRain = d.properties.Jul;
        }
    if (monthSelect == months[2]) {
            textRain = d.properties.Aug;
        }
    if (monthSelect == months[3]) {
            textRain = d.properties.Sep;
        }
    if (monthSelect == months[4]) {
            textRain = d.properties.Oct;
        }
    if (monthSelect == months[5]) {
            textRain = d.properties.Nov;
        }
    if (monthSelect == months[6]) {
            textRain = d.properties.Dec;
        }
    if (monthSelect == months[7]) {
            textRain = d.properties.Jan;
        }
    if (monthSelect == months[8]) {
            textRain = d.properties.Feb;
        }
    if (monthSelect == months[9]) {
            textRain = d.properties.Mar;
        }
    if (monthSelect == months[10]) {
            textRain = d.properties.Apr;
        }
    if (monthSelect == months[11]) {
            textRain = d.properties.May;
    }
    } else {
        // code for deficit labels, add % sign
     if (monthSelect == months[0]) {
            textRain = "0"; // hhhhd.properties.decJun;
        }
    if (monthSelect == months[1]) {
            textRain = d.properties.decJul;
        }
    if (monthSelect == months[2]) {
            textRain = d.properties.decAug;
        }
    if (monthSelect == months[3]) {
            textRain = d.properties.decSep;
        }
    if (monthSelect == months[4]) {
            textRain = d.properties.decOct;
        }
    if (monthSelect == months[5]) {
            textRain = d.properties.decNov;
        }
    if (monthSelect == months[6]) {
            textRain = d.properties.decDec;
        }
    if (monthSelect == months[7]) {
            textRain = d.properties.decJan;
        }
    if (monthSelect == months[8]) {
            textRain = d.properties.decFeb;
        }
    if (monthSelect == months[9]) {
            textRain = d.properties.decMar;
        }
    if (monthSelect == months[10]) {
            textRain = d.properties.decApr;
        }
    if (monthSelect == months[11]) {
            textRain = d.properties.decMay;
    }
    }
    return d.properties.NSW_LGA__2 + ": " + textRain + " mm";
}

/**
 * mouse over tool tip
 * @param {obj} d d3 obj.
 */
function mouseover(d) {
    // Highlight hovered province
    d3.select(this).style('fill', 'orange');
    d3.select("#tooltip")
    .select("#value")
    .text(nameFn(d));
    d3.select("#tooltip").classed("hidden", false);
}

/**
 * Redraws colors again
 * @param {obj} d d3 obj.
 */
function mouseout(d) {
    changeMonth(monthSelect);
}

/**
 * Redraws colors again
 * @param {string} month string.
 */
function changeMonth(month) {
    // Reset need a way of updating Not working yet
    // d3.select(this).style('fill', colorMean(d.properties.mean));
    mapLayer.selectAll('path')
    .attr('fill', '#fff')
    .attr('stroke', '#aaa')
    .attr('vector-effect', 'non-scaling-stroke')
    .style('fill', function(d) {
        var formVal;
        for (var i=0; i<form.length; i++) {
           if (form[i].checked) {
              formVal = form[i].id;
              }
        }
        if (formVal == 'mean') {
        if (month == months[0]) {
            return colorMean(d.properties.Jun);
        }
        if (month == months[1]) {
            return colorMean(d.properties.Jul);
        }
        if (month == months[2]) {
            return colorMean(d.properties.Aug);
        }
        if (month == months[3]) {
            return colorMean(d.properties.Sep);
        }
        if (month == months[4]) {
            return colorMean(d.properties.Oct);
        }
        if (month == months[5]) {
            return colorMean(d.properties.Nov);
        }
        if (month == months[6]) {
            return colorMean(d.properties.Dec);
        }
        if (month == months[7]) {
            return colorMean(d.properties.Jan);
        }
        if (month == months[8]) {
            return colorMean(d.properties.Feb);
        }
        if (month == months[9]) {
            return colorMean(d.properties.Mar);
        }
        if (month == months[10]) {
            return colorMean(d.properties.Apr);
        }
        if (month == months[11]) {
            return colorMean(d.properties.May);
        } else {
            return "#ccc";
        }
        } else {
            // code to deal with deficit choice on radio
        if (month == months[0]) {
            return colorDecrease(d.properties.decJun);
        }
        if (month == months[1]) {
            return colorDecrease(d.properties.decJul);
        }
        if (month == months[2]) {
            return colorDecrease(d.properties.decAug);
        }
        if (month == months[3]) {
            return colorDecrease(d.properties.decSep);
        }
        if (month == months[4]) {
            return colorDecrease(d.properties.decOct);
        }
        if (month == months[5]) {
            return colorDecrease(d.properties.decNov);
        }
        if (month == months[6]) {
            return colorDecrease(d.properties.decDec);
        }
        if (month == months[7]) {
            return colorDecrease(d.properties.decJan);
        }
        if (month == months[8]) {
            return colorDecrease(d.properties.decFeb);
        }
        if (month == months[9]) {
            return colorDecrease(d.properties.decMar);
        }
        if (month == months[10]) {
            return colorDecrease(d.properties.decApr);
        }
        if (month == months[11]) {
            return colorDecrease(d.properties.decMay);
        } else {
            return "#ccc";
        }
        }
    });
}
console.log('test2');
