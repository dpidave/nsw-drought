
# D3 based drought map of NSW July 2017 to 2018.  
End goal, a dynamic map of nsw local govenment areas with the following data
-  cumulitavive per month change in rainfall from long term average (running
   total) 
-  per month, deveation from montly mean
-  Year long average rainfall (radio)

## UI  
-  Slider at the top of the page scrolling through the 12 months july to july  
-  Radio button for the above 3 catories  


## interation 1
Really good example here: https://bl.ocks.org/john-guerra/43c7656821069d00dcbc
-  load NSW shapefile

code used (I just ended up using an online map.org code to shrink)   
```ogr2ogr -f GeoJSON -lco COORDINATE_PRECISION=2 -t_srs EPSG:4326 nsw_lga_sm.geo.json NSW_LGA_POLYGON_shp.shp```
```geo2topo -o nsw_lga_sm.geo.tson name=nsw_lga_sm.geo.json```

## iteration 2
-  add tool tils and mouse overs

## ToDo  
~~Fix mouse over, rainfall missing and placement wrong~~  
~~check the starting colors I dont think they are correct, maybe out by one month?~~  
~~Fix update on radio button push, colors should update to deficit etc~~  
~~Fix date labels on radio push, means should not have year (also add monthly)~~  
~~Fix slider with css~~  
Move float to follow mouse  
~~Remove LGA region from label~~  
Think of a better metric than rainfall deficit  
Add color key that changes with radio button   
